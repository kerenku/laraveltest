<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('customers','CustomerController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Auth::routes(['verify' => true, 'register' => false]);




Route::get('customers/delete/{id}', 'CustomerController@destroy')->name('delete');
Route::get('customers/sold/{id}', 'CustomerController@close')->name('close');

