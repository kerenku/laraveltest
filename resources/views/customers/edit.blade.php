@extends('layouts.app')
@section('content')
    
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        </head>
     <!--Update-->

        <div class="container">
            <br/><br/>
            <h3>Edit Customer</h3>
           
            
            <form method="post" action ="{{action('CustomerController@update' , $customer->id)}}">
                @csrf
                @method('PATCH')
                
                    <div class="form-group">
                        <label for="title">Customer to update</label>
                        <input type ="text" class ="form-control" name="name" value = "{{$customer->name}}">
                    </div>
                   
                    <div class="form-group">
                        <label for="title">Email to update</label>
                        <input type ="text" class ="form-control" name="email" value = "{{$customer->email}}">
                    </div>

                     <div class="form-group">
                        <label for="title">Phone to update</label>
                        <input type ="text" class ="form-control" name="phone" value = "{{$customer->phone}}">
                    </div>

                    <!-- @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                  @endif -->
                    
                    <br>
                    <div class ="container">
                        <div class="col-4  offset-4">
                            <input type ="submit" class="form-control btn btn-secondary" name="submit" value =" save "> 
                        </div>
                    </div>
            </form>
        </div>
        
        
       
        @endsection