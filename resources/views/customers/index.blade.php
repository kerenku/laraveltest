@extends('layouts.app')
@section('content')
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=3, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<div class='container'>
    <br/><br/>
<h1>Customers list</h1>

 <h3><a href = "{{route('customers.create')}}">Create new customers </a></h3>

<table class="table">
<tr>
    <th>Name</th>
    <th>Email </th>
    <th>Phone</th>
    <th>Who create?</th>
    <th>Status</th> 
    <th>Edit</th>
    @can('manager')
    <th>Delete</th>

    @endcan
</tr>

@foreach($customers as $customer)
<tr> 

<td> {{$customer->name}}</td> 
<td> {{$customer->email}} </td>
<td> {{$customer->phone}}</td> 
<td> @if ($customer->user_id==1)  <a>aa</a>@endif
     @if ($customer->user_id==2)  <a>bb</a>@endif
     @if ($customer->user_id==3 ) <a>cc</a>@endif
     
</td>
<td>
    @if ($customer->status==0)
   
    @can('manager')<a href="{{route('close', $customer->id)}}">deal closed</a>  @endcan
    
    @endif
</td>
<td><a href = "{{route('customers.edit',$customer->id)}}">Edit</a></td>
<td>
@can('manager')<a href="{{route('delete', $customer->id)}}">Delete</a>@endcan
@can('salesrep')<a >Delete</a>@endcan
</td>
</tr>

@endforeach
</table>

</div>






   @endsection