@extends('layouts.app')
@section('content')
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<div class="container">
    <br/><br/>
    <h3> Add a new customer</h3>
        <!-- @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif -->

    <form method="post" action ="{{action('CustomerController@store')}}">
        {{csrf_field()}}
            <div class="form-group">
                <label for="title"> Name </label>
                <input type ="text" class ="form-control" name="name">
            </div>
            <div class="form-group">
                <label for="title"> Email </label>
                <input type ="text" class ="form-control" name="email">
            </div>
            <div class="form-group">
                <label for="title"> Phone </label>
                <input type ="text" class ="form-control" name="phone">
            </div>
            
         
         
            <div class ="container">
                <div class="col-4  offset-4">
                     <input type ="submit" class="form-control btn btn-secondary" name="submit" value ="Create"> 
                </div>
            </div> 
        <br><br>
        <div class ="container">
            <div class="col-4  offset-4">
                <a href="{{route('customers.index')}}" class=" form-control btn btn-secondary">Back to list</a>
            </div>
        </div>
  </div>

    </form>
</div>

@endsection

