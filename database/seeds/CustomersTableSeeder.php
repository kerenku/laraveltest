<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert(
            [    
                [
                'name' => 'Dina',
                'email' => 'Dina@gmail.com',
                'phone' => 050241533,
                'user_id' => 1,
                'status'=>0,
                'created_at' => date('Y-m-d G:i:s')
                ],
                [
                    'name' =>'Moshe',
                    'email' => 'Moshe@gmail.com',
                    'phone' => 050245623,
                    'user_id' => 1,
                    'status'=>0,
                    'created_at' => date('Y-m-d G:i:s')
                ],
                [
                    'name' => 'Yossi',
                    'email' => 'Yossi@gmail.com',
                    'phone' => 050245000,
                    'user_id' => 2,
                    'status'=>0,
                    'created_at' => date('Y-m-d G:i:s')
                ],
             
                
            ]
            );
    }
}
